{-# LANGUAGE ScopedTypeVariables #-}
module Streaming.Csv
  ( -- * Decoding
    -- ** Index-based record conversion
    decode
  , decodeWith
    -- ** Name-based record conversion
  , decodeByName
  , decodeByNameWith
    -- ** Low-level interface
  , decodeRecords
    -- * Encoding
    -- ** Index-based record conversion
  , encode
  , encodeWith
    -- ** Name-based record conversion
  , encodeByName
  , encodeByNameWith
  ) where
import qualified Data.Attoparsec.ByteString.Char8     as A8
import qualified Data.Attoparsec.ByteString.Streaming as SA
import           Data.ByteString.Streaming            (ByteString)
import qualified Data.ByteString.Streaming            as B
import           Data.Csv                             (DecodeOptions (..),
                                                       EncodeOptions,
                                                       FromNamedRecord (..),
                                                       FromRecord (..),
                                                       HasHeader (..), Record)
import qualified Data.Csv                             as Csv
import qualified Data.Csv.Parser                      as CP
import qualified Data.Vector                          as V
import           Data.Word                            (Word8)
import qualified Pipes                                as P
import qualified Pipes.Csv                            as PCsv
import qualified Pipes.Prelude                        as P
import           Streaming
import qualified Streaming.Prelude                    as S

--------------------------------------------------------------------------------
-- Decoding

-- | Streaming equivalent of 'Csv.decode'
decode
  :: (Monad m, FromRecord a)
  => HasHeader
  -> ByteString m r
  -> Stream (Of (Either String a)) m ()
decode = decodeWith Csv.defaultDecodeOptions

-- | Streaming equivalent of 'Csv.decode'
decodeWith
  :: (Monad m, FromRecord a)
  => DecodeOptions
  -> HasHeader
  -> ByteString m r
  -> Stream (Of (Either String a)) m ()
decodeWith deopts hh bs = S.map
  (Csv.runParser . Csv.parseRecord)
  (decodeWith' deopts hh bs)

-- | Streaming equivalent of 'Csv.decodeWith'
decodeWith'
  :: forall m r. Monad m
  => DecodeOptions
  -> HasHeader
  -> ByteString m r
  -> Stream (Of Record) m ()
decodeWith' DecodeOptions{decDelimiter = delim} hasHeader bs =
  case hasHeader of
    NoHeader ->
      () <$ decodeRecords delim bs
    HasHeader -> do
      (msg', remaining) <- lift (SA.parse (CP.header delim) bs)
      case msg' of
        Left  r -> do
          S.yield r
          () <$ decodeRecords delim remaining
        Right (_, _er) ->
          return ()

  -- | Streaming equivalent of 'Csv.decodeByName'
decodeByName
  :: (Monad m, FromNamedRecord a)
  => ByteString m r
  -> Stream (Of (Either String a)) m ()
decodeByName = decodeByNameWith Csv.defaultDecodeOptions

-- | Streaming equivalent of 'Csv.decodeByNameWith'
decodeByNameWith
  :: (Monad m, FromNamedRecord a)
  => DecodeOptions
  -> ByteString m r
  -> Stream (Of (Either String a)) m ()
decodeByNameWith DecodeOptions{decDelimiter = delim} bs = do
  (msg', remaining) <- lift (SA.parse (CP.header delim) bs)
  case msg' of
    Left header -> () <$ S.map
      (Csv.runParser
       . Csv.parseNamedRecord
       . Csv.namedRecord
       . V.toList
       . V.zip header)
      (decodeRecords delim remaining)
    Right _ -> return ()

decodeRecords :: Monad m
              => Word8
              -> ByteString m r
              -> Stream (Of Record) m (Either (SA.Message, ByteString m r) r)
decodeRecords delim =
  SA.parsed (CP.record delim <* (A8.endOfLine <|> A8.endOfInput))

--------------------------------------------------------------------------------
-- Encoding

encode :: (Csv.ToRecord a, Monad m)
       => Stream (Of a) m r
       -> ByteString m ()
encode = B.mwrap . S.fold_
  (\xs x -> xs >> B.fromLazy (Csv.encode [x]))
  (return ())
  id

encodeWith
  :: (Csv.ToRecord a, Monad m)
  => EncodeOptions
  -> Stream (Of a) m r
  -> ByteString m ()
encodeWith opts = B.mwrap . S.fold_
  (\xs x -> xs >> B.fromLazy (Csv.encodeWith opts [x]))
  (return ())
  id

encodeByName
  :: (Csv.ToNamedRecord a, Monad m)
  => Csv.Header
  -> Stream (Of a) m r
  -> ByteString m r
encodeByName h s =
  B.fromChunks $
  S.unfoldr P.next $
  P.unfoldr S.next s P.>-> PCsv.encodeByName h

encodeByNameWith
  :: (Csv.ToNamedRecord a, Monad m)
  => Csv.EncodeOptions
  -> Csv.Header
  -> Stream (Of a) m r
  -> ByteString m r
encodeByNameWith encopts h s =
  B.fromChunks $
  S.unfoldr P.next $
  P.unfoldr S.next s P.>-> PCsv.encodeByNameWith encopts h

